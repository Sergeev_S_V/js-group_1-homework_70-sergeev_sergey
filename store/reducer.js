import * as actionTypes from '../store/actionTypes';

const initialState = {
  value: '',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.PRESSED:
      return {value: state.value += action.value};
    case actionTypes.RESULT:
      if (state.value.length > 0) {
        try {
          return {value: String(eval(state.value))};
        } catch (e) {
          alert('Error!');
        }
      }
      return state;
    case actionTypes.REMOVE:
      return {value: state.value.substring(0, state.value.length - 1)};
    case actionTypes.CLEAR:
      return {value: ''};
    default:
      return state;
  }
};

export default reducer;