import * as actionTypes from './actionTypes';

export const pressed = value => {
  return {type: actionTypes.PRESSED, value};
};

export const getResult = () => {
  return {type: actionTypes.RESULT};
};

export const removeOneSymbol = () => {
  return {type: actionTypes.REMOVE};
};

export const clearValue = () => {
  return {type: actionTypes.CLEAR};
};