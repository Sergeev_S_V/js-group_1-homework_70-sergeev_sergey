import React, {Component} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {connect} from "react-redux";

import {clearValue, getResult, pressed, removeOneSymbol} from "../store/actions";

const symbols = [
    {type: 'number', name: '1'},
    {type: 'number', name: '2'},
    {type: 'number', name: '3'},
    {type: 'action', name: '+'},
    {type: 'number', name: '4'},
    {type: 'number', name: '5'},
    {type: 'number', name: '6'},
    {type: 'action', name: '-'},
    {type: 'number', name: '7'},
    {type: 'number', name: '8'},
    {type: 'number', name: '9'},
    {type: 'action', name: '/'},
    {type: 'number', name: '0'},
    {type: 'quality', name: '='},
    {type: 'remove', name: '<'},
    {type: 'action', name: '*'},
    {type: 'clear', name: 'CE'}
  ];

class Calculator extends Component {

  onPressed = symbol => {
    symbol.type === 'number' || symbol.type === 'action'
      ? this.props.pressed(symbol.name)
      : null;
    symbol.type === 'quality'
      ? this.props.getResult(symbol.name)
      : null;
    symbol.type === 'remove'
      ? this.props.removeOneSymbol(symbol.name)
      : null;
    symbol.type === 'clear'
      ? this.props.clearValue(symbol.name)
      : null;
  };

  render() {
    return(
      <View style={styles.container}>
        <Text style={styles.result}>{this.props.value}</Text>
        <View style={styles.symbols}>
          {symbols.map(symbol => (
            <TouchableOpacity style={styles.numbers}
                              key={symbol.name}
                              onPress={() => this.onPressed(symbol)}>
              <Text style={{fontSize: 30}}>{symbol.name}</Text>
            </TouchableOpacity>
          ))}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  symbols: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  numbers: {
    width: '25%',
    height: '20%',
    borderWidth: 1,
    borderColor: 'black',
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  result: {
    marginTop: 40,
    marginBottom: 40,
    textAlign: 'right',
    fontSize: 70,
    height: 100,
  },
});

const mapStateToProps = state => {
  return {
    value: state.value,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    pressed: symbol => dispatch(pressed(symbol)),
    getResult: () => dispatch(getResult()),
    removeOneSymbol: () => dispatch(removeOneSymbol()),
    clearValue: () => dispatch(clearValue()),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Calculator);