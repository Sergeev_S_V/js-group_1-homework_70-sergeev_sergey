import React from 'react';
import {applyMiddleware, createStore} from "redux";
import thunkMiddleware from 'redux-thunk';
import {Provider} from "react-redux";

import reducer from "./store/reducer";
import Calculator from "./Components/Calculator";

const store = createStore(reducer, applyMiddleware(thunkMiddleware));

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Calculator/>
      </Provider>
    );
  }
}


